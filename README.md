# INF01048 - ARTIFICIAL INTELIGENCE #
at UFRGS Brazil, Lectured bt Dr. Paulo Martins Engel

It the final projects a bot with artificial inteligencie was created to simulate a player in a Pawn War game. The goal of the players is to get one of your pawns to cross the entire boar and reach the end, while avoiding that your opponent do the same. In this version, besides pawns, Rooks and Bishops were used.

In order to create the boot we used a minimax tree with alpha-beta pruning to evaluate all the possible combinations of plays in the next 3 moventes, evaluating all the possible future states before making a decision. 

To evaluate each state, we created a heuristic evaluation function based on 4 relevant information:

 * The number of passed pawns(pawn that has a free "lane" ahead with no piece on the way); 
 * Material advantage, where a value is given for each piece, rooks and bishops have higher value;
 * Quality of each Bishop;
 * Quality of each Rook;

Additional descriptions can be seen in the pdf in the folder(in portuguese)

   